package com.example.demo.entity;

public class Catalog {
	
	private String name;
	private String review;
	
	
	public Catalog(String name, String review) {
		super();
		this.name = name;
		this.review = review;
	}
	
	public Catalog() {
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getReview() {
		return review;
	}
	public void setReview(String review) {
		this.review = review;
	}

}
