package com.example.demo.entity;

public class order {
	private int orderid;
	private String ordername;
	
	public order() {
		
	}
	public order(int orderid, String ordername) {
		super();
		this.orderid = orderid;
		this.ordername = ordername;
	}
	public int getOrderid() {
		return orderid;
	}
	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}
	public String getOrdername() {
		return ordername;
	}
	public void setOrdername(String ordername) {
		this.ordername = ordername;
	}
	

}
