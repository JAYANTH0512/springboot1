package com.example.demo.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.Feigns;
import com.example.demo.entity.Catalog;
import com.example.demo.entity.order;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
@RequestMapping("/catalog")

public class Controller {
	
	@Value("${catlog.name}")
	private String a;
	
//	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(Controller.class);
private  static final Logger LOG = LoggerFactory.getLogger(Controller.class);	
	
//	@Autowired
//	public RestTemplate resttemplate;

	
	  @GetMapping("/info")
      public Catalog getcataloginfo() {
    	  return new Catalog("Lappy","Good");
      }
	  
//	  @GetMapping("/information")
//	  public order getOrderInfo() {
//	  order order = resttemplate.getForObject("https://Gateway-API/orders/orders/information", order.class);	
//	  return order;
//	}
	  
	  
	  
	  @Autowired
	  private Feigns feigns;
	  
	  @GetMapping("/Orders/information")
	  @HystrixCommand(fallbackMethod = "getduplicateorder")
	  public order getOrderInfo() {
			return feigns.getorderinfo();
		}
	  
	  public order getduplicateorder() {
		  return new order(5,"Mobile");
	  }
	  @GetMapping("/String")
	  public String getString()
	  {
		  return a;
	  }
}
