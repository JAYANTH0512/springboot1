package com.example.demo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.example.demo.entity.order;



@FeignClient(name="Orders")
public interface Feigns {
	
	@GetMapping("/orders/information")
	public order getorderinfo();

}
