package com.example.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.order;

@RestController
@RequestMapping("/orders")
public class Controller {
	
	
	private static final Logger Log = LoggerFactory.getLogger(Controller.class);
	
	@GetMapping("/information")
	public order getorderinfo() {
		return new order(1,"lappy");
	}
}
